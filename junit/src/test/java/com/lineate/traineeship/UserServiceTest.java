package com.lineate.traineeship;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class UserServiceTest {

    private UserService userService;

    @BeforeEach
    private void createService() {
        ServiceFactory serviceFactory = new ServiceFactory();
        userService = serviceFactory.createUserService();
    }

    @Test
    public void createGroupTest() {
        List<Permission> perm = Arrays.asList(Permission.write, Permission.read, Permission.read);
        Group group1 = userService.createGroup("1A", perm);
        Group group2 = userService.createGroup("2A", perm);
        User user1 = userService.createUser("Oleg", group1);
        User user2 = userService.createUser("Andrey", group1);
        User user3 = userService.createUser("Ivan", group2);
        group1.addUser(user3);
        Assertions.assertAll("check group",
                () -> Assertions.assertNotNull(group1.getName()),
                () -> Assertions.assertTrue(group1.getPermissions().size() == 2),
                () -> Assertions.assertTrue(group1.getUsers().size() == 3)
        );
    }

    @Test
    public void createGroupWithEmptyNameTest() {
        List<Permission> perm = Arrays.asList(Permission.write, Permission.read);
        Group group = userService.createGroup("", perm);
        Assertions.assertNull(group);
    }

    @Test
    public void createGroupWithOutPermTest() {
        Group group = userService.createGroup("1A", null);
        Assertions.assertNull(group);
    }

    @Test
    public void createUserTest() {
        List<Permission> perm = Arrays.asList(Permission.write);
        Group group = userService.createGroup("1A", perm);
        User user = userService.createUser("Oleg", group);
        Assertions.assertAll("check user",
                () -> Assertions.assertNotNull(user.getName()),
                () -> Assertions.assertTrue(user.getGroups().size() > 0)
        );
    }

    @Test
    public void createUserTestWithOutGroupTest() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            User user = userService.createUser("Oleg", null);
        });

    }

    @Test
    public void createUserTestWithEmptyNameTestFail() {
        List<Permission> perm = Arrays.asList(Permission.read);
        Group group = userService.createGroup("1A", perm);
        User user = userService.createUser("", group);
        Assertions.assertNull(user);
    }
}
