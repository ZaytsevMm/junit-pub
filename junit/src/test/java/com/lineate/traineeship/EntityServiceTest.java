package com.lineate.traineeship;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;

public class EntityServiceTest {

    private UserService userService;
    private EntityService entityService;

    @BeforeEach
    private void createServices() {
        ServiceFactory serviceFactory = new ServiceFactory();
        userService = serviceFactory.createUserService();
        entityService = serviceFactory.createEntityService();
    }

    @ParameterizedTest
    @ValueSource(strings = {"", "Abc ", " Abc", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "\t", "\n"})
    public void createEntityTestName(String arg) {
        List<Permission> perm = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("1A", perm);
        User user = userService.createUser("Oleg", group);
        Assertions.assertFalse(entityService.createEntity(user, arg, "value"));
    }

    @Test
    public void createEntityTestOwner() {
        List<Permission> perm = Arrays.asList(Permission.read);
        Group group = userService.createGroup("1A", perm);
        User user = userService.createUser("Oleg", group);
        entityService.createEntity(user, "Product", "clock v1");
        Assertions.assertAll(
                () -> Assertions.assertTrue(entityService.updateEntity(user, "Product", "clock v2")),
                () -> Assertions.assertEquals(entityService.getEntityValue(user, "Product"), "clock v2")
        );
    }

    @Test
    public void createEntityTestNotOwner_And_ReadPerm() {
        List<Permission> perm1 = Arrays.asList(Permission.read);
        List<Permission> perm2 = Arrays.asList(Permission.write);
        Group group1 = userService.createGroup("1A", perm1);
        Group group2 = userService.createGroup("2A", perm2);
        User user1 = userService.createUser("Oleg", group1);
        User user2 = userService.createUser("Andrey", group2);
        group1.addUser(user2);
        entityService.createEntity(user1, "Product", "clock v1");
        Assertions.assertAll(
                () -> Assertions.assertNotNull(entityService.getEntityValue(user2, "Product")),
                () -> Assertions.assertFalse(entityService.updateEntity(user2, "Product", "clock v0"))
        );
    }

    @Test
    public void createEntityTestNotOwner_And_OnlyWritePerm() {
        List<Permission> perm = Arrays.asList(Permission.write);
        Group group = userService.createGroup("1A", perm);
        User user1 = userService.createUser("Oleg", group);
        User user2 = userService.createUser("Andrey", group);
        entityService.createEntity(user1, "Product", "clock v1");
        Assertions.assertAll("check perm user2",
                () -> Assertions.assertNull(entityService.getEntityValue(user2, "Product")),
                () -> Assertions.assertTrue(entityService.updateEntity(user2, "Product", "clock v0"))
        );
    }

    @Test
    public void createEntityTestWitOutGroup() {
        List<Permission> perm = Arrays.asList(Permission.read, Permission.write);
        Group group1 = userService.createGroup("1A", perm);
        Group group2 = userService.createGroup("2A", perm);
        User user1 = userService.createUser("Oleg", group1);
        User user2 = userService.createUser("Andrey", group2);
        entityService.createEntity(user1, "Product", "clock v1");
        Assertions.assertAll("check perm user2",
                () -> Assertions.assertNull(entityService.getEntityValue(user2, "Product")),
                () -> Assertions.assertFalse(entityService.updateEntity(user2, "Product", "clock v0"))
        );
    }
}
