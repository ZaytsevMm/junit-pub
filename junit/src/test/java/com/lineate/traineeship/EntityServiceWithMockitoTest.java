package com.lineate.traineeship;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;

public class EntityServiceWithMockitoTest {

    private UserService userService;
    private EntityService entityService;
    private EntityRepository entityRepository;

    @BeforeEach
    private void createServices() {
        ServiceFactory serviceFactory = new ServiceFactory();
        userService = serviceFactory.createUserService();
        entityRepository = Mockito.mock(EntityRepository.class);
        entityService = serviceFactory.createEntityService(entityRepository);
    }

    @Test
    public void getEntityTest() {
        Group group = userService.createGroup("1A", Arrays.asList(Permission.read));
        User user = userService.createUser("Andrey", group);
        Entity entity = new Entity(user, "SomeEntity");
        entity.setValue("Entity value");
        Mockito.when(entityRepository.get("SomeEntity")).thenReturn(entity);
        entityService.createEntity(user, "SomeEntity", "Entity value");
        Assertions.assertEquals("Entity value", entityService.getEntityValue(user, "SomeEntity"));
        Mockito.verify(entityRepository, Mockito.times(1)).save(entity);
    }

    @Test
    public void updateEntityTest() {
        Group group = userService.createGroup("1A", Arrays.asList(Permission.read, Permission.write));
        User user = userService.createUser("Andrey", group);
        Entity entity = new Entity(user, "SomeEntity");
        entity.setValue("Entity value");
        Mockito.when(entityRepository.get("SomeEntity")).thenReturn(entity);
        entityService.createEntity(user, "SomeEntity", "Entity value");
        Assertions.assertAll("check update",
                () -> Assertions.assertTrue(entityService.updateEntity(user, "SomeEntity", "just value")),
                () -> Assertions.assertEquals("just value",
                        entityService.getEntityValue(user, "SomeEntity"))
        );
        Mockito.verify(entityRepository, Mockito.times(1)).save(entity);
    }
}
